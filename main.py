import uvicorn

if __name__=="__main__":
    uvicorn.run(
        "API.api:app",
        host="0.0.0.0",
        port=8000,
        reload=True,
        ssl_keyfile="./certs/server.key",
        ssl_certfile="./certs/server.pem"
    )
