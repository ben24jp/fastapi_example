from sqlalchemy.orm import Session
from . import models, schemas

async def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id==user_id).first()

async def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email==email).first()

async def get_users(db: Session, skip: int=0, limit: int=100):
    return db.query(models.User).offset(skip).limit(limit).all()

async def create_user(db: Session, user: schemas.UserCreate):
    fake_hashes_password=user.password + "notreallyhashes"
    db_user=models.User(email=user.email, hashed_password=fake_hashes_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

async def delete_user_by_id(db: Session, user_id: int):
    db_user=db.query(models.User).filter(models.User.id==user_id).first()
    if db_user==None:
        return None
    else:
        db.delete(db_user)
        db.commit()
        return db_user

async def update_a_user(db: Session, user: schemas.UserUpdate):
    db_user=db.query(models.User).filter(models.User.id==user.id).first()
    if db_user==None:
        return None
    else:
        db_user.email=user.email
        db_user.password=user.password
        db_user.is_active=user.is_active
        db.merge(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user
