from typing import List
from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
from API import models
from API import schemas
from API import crud
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app=FastAPI()

origins=[
    "http://localhost:3000",
    "localhost:3000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

def get_db():
    db=SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/user", response_model=schemas.User)
async def create_user(user: schemas.UserCreate, db: Session=Depends(get_db)):
    db_user=await crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return await crud.create_user(db=db, user=user)

@app.get("/users", response_model=List[schemas.User])
async def read_users(skip: int=0, limit: int=100, db: Session=Depends(get_db)):
    users=await crud.get_users(db, skip=skip, limit=limit)
    return users

@app.get("/user/{user_id}", response_model=schemas.User)
async def read_user(user_id: int, db: Session=Depends(get_db)):
    db_user=await crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@app.delete("/user/{user_id}", response_model=schemas.User)
async def delete_user(user_id: int, db: Session=Depends(get_db)):
    db_user=await crud.delete_user_by_id(user_id=user_id, db=db)
    if db_user:
        return db_user
    else:
        raise HTTPException(status_code=404, detail="User not found")

@app.patch("/user", response_model=schemas.User)
async def update_user(user: schemas.UserUpdate, db: Session=Depends(get_db)):
    db_user=await crud.update_a_user(db=db, user=user)
    if db_user:
        return db_user
    else:
        raise HTTPException(status_code=404, detail="User not found")
