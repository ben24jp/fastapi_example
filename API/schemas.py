from pydantic import BaseModel

class UserBase(BaseModel):
    email: str

class UserCreate(UserBase):
    password: str

class UserUpdate(UserBase):
    id: int
    password: str
    is_active: bool

class User(UserBase):
    id: int
    is_active: bool=False

    class Config:
        orm_mode = True
